@if($team->hasSubscription())
<p>You're used {{ $team->users->count() }} out of {{ optional($team->plan)->teams_limit ?? '0'}} availiable user slots.</p>
@endif