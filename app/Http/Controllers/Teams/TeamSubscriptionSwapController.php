<?php

namespace App\Http\Controllers\Teams;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Team;
use App\Plan;

class TeamSubscriptionSwapController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware(['in_team:' . $request->team]);
        $this->middleware(['permission:manage team subscription']);
    }
    public function store(Request $request, Team $team)
    {
        $this->validate($request, [
          'plan' => 'required|exists:plans,id'
        ]);

        $plan = Plan::find($request->plan);

        if(!$team->canDonwgrade($plan)) {
            return back();
        }

        $team->currentSubscription()->swap(
          $plan->provider_id
        );

        return back();
    }
}
