<?php

namespace App;

use Laravel\Cashier\Subscription;
use App\Team;

class TeamSubscription extends Subscription
{
    public function owner()
    {
        return $this->belongsTo(Team::class, (new Team)->getForeignKey());
    }
}
